import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WriteFileService {
  dataFormJson : any;
  constructor() { }

  saveDataLocal(data) {
    this.dataFormJson = data; 
  }
}
