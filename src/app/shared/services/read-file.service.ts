import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ReadFileService {
    isOpen: boolean = false;
    selectFile: File;
    dataJsonForm;
    constructor(private router: Router) { }
    readFile(event) {
        let promise = new Promise(resolve => {
            this.selectFile = event.target.files[0];
            const fileReader = new FileReader();
            fileReader.readAsText(this.selectFile, 'utf-8');
            fileReader.onload = () => {
                let dataForm = JSON.parse(fileReader.result as string);                
                this.dataJsonForm = dataForm;
                this.isOpen = true;
                resolve("ok");
            }
        });
        promise.then(res => {
            this.router.navigateByUrl('/preview')
        });
    }
}
