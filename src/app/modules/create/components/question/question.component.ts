import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { WriteFileService } from '../../../../shared/services/write-file.service';

@Component({
    selector: 'app-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
    @ViewChild('listAnswer') listAnswer : ElementRef;
    @ViewChild('formQuestion') formQuestion : ElementRef;
    @ViewChild('titleForm') titleForm : ElementRef;
    typeInput: any = [
        {
            value: 'text',
            textValue: 'Trả lời ngắn'
        },
        {
            value: 'radio',
            textValue: 'Trắc nghiệm'
        },
        {
            value: 'checkbox',
            textValue: 'Hộp kiểm'
        },
    ];
    number : number = 0;
    arrOptionOfQuestion : Array<any> = [];
    constructor(private router: Router, private writeFileService : WriteFileService) { }

    ngOnInit(): void {
        
    }
    addQuestion() {
        let objQuestion = {
            nameQuestion: '',
            optionQuestion: [],
            type: 'radio'
        }
        this.arrOptionOfQuestion.push(objQuestion);
        //console.log(this.arrOptionOfQuestion);
    }

    addAnswer(type, index) {  
        // === update arr option === //
        if(type == 'radio') {
            this.arrOptionOfQuestion[index].optionQuestion.push(`Tuỳ chọn ${++this.number}`);
        }
        if(type == 'checkbox') {
            this.arrOptionOfQuestion[index].optionQuestion.push(`Tuỳ chọn ${++this.number}`);
        }       
        // ================================================================//
    }

    onChangeTypeInput(e, index) {
        // === update type === //
        this.arrOptionOfQuestion[index].type = e.value;
        // === reset option === //
        this.number = 0;     
        this.arrOptionOfQuestion[index].optionQuestion = [];
    }

    editOption(e, indexArrQuestion, indexArrOption) {
        let value = e.target.value;
        if(e.code == "Enter") {
            this.arrOptionOfQuestion[indexArrQuestion].optionQuestion[indexArrOption] = value;
        }       
    }
    onblurEditOption(e, indexArrQuestion, indexArrOption) {
        let value = e.target.value;
        this.arrOptionOfQuestion[indexArrQuestion].optionQuestion[indexArrOption] = value;
    }

    editNameQuestion(e, indexArrQuestion) {
        let value = e.target.value;
        if(e.code == "Enter") {
            this.arrOptionOfQuestion[indexArrQuestion].nameQuestion = value;
        }  
        // e.target.onblur = function() {
        //     this.arrOptionOfQuestion[indexArrQuestion].nameQuestion = value;
        // }
    }
    onblurEditNameQuestion(e, index) {
        this.arrOptionOfQuestion[index].nameQuestion = e.target.value;
    }

    onDeleteOption() {
        
    }

    saveForm() {
        let titleForm = this.titleForm.nativeElement;

        let listQuestion = {
            form : {
                question: {
                    titleForm: titleForm.value,
                    arrListQuestion: this.arrOptionOfQuestion
                }
            }
        }
        
        var data  = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(listQuestion)); 
   
        var a = document.createElement('a'); 
        a.href = 'data:' + data; 
        a.download = 'datatest.json'; 
        a.innerHTML = 'download'; 
        a.click();

        //console.log(listQuestion);
    }

    onPreview() {
        // === update data ===//       

        let titleForm = this.titleForm.nativeElement;
        let listQuestion = {
            form : {
                question: {
                    titleForm: titleForm.value,
                    arrListQuestion: this.arrOptionOfQuestion
                }
            }
        }       
       
        this.writeFileService.saveDataLocal(listQuestion);
        this.router.navigateByUrl('/preview');       
    }
}
