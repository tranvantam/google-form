import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';
import { SharedModule } from '../../shared/shared.module';
import { QuestionComponent } from './components/question/question.component';
import { AnswerComponent } from './components/answer/answer.component';
import { FormQuestionComponent } from './components/form-question/form-question.component';


@NgModule({
  declarations: [CreateComponent, QuestionComponent, AnswerComponent, FormQuestionComponent],
  imports: [
    CommonModule,
    CreateRoutingModule,
    SharedModule,
    RouterModule
  ]
})
export class CreateModule { }
