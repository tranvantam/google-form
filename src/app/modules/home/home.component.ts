import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ReadFileService } from '../../shared/services/read-file.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    @ViewChild('form') form : ElementRef; 
    constructor(private readFileService: ReadFileService) { }

    ngOnInit(): void {
    }
    onChangeInput(e) {
        this.readFileService.readFile(e);
    }
}
