import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PreviewRoutingModule } from './preview-routing.module';
import { PreviewComponent } from './preview.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [PreviewComponent],
  imports: [
    CommonModule,
    PreviewRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class PreviewModule { }
