import { Component, OnInit } from '@angular/core';
import { WriteFileService } from '../../shared/services/write-file.service';
import { ReadFileService } from '../../shared/services/read-file.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  data : any;
  arrQuestion: Array<any>;
  
  constructor(private writeFileService: WriteFileService, private readFileService: ReadFileService) { }

  ngOnInit(): void {
    if(this.readFileService.isOpen) {
      this.data = this.readFileService.dataJsonForm;
      this.arrQuestion = this.readFileService.dataJsonForm.form.question.arrListQuestion;
      this.readFileService.isOpen = false;
    } else {
      this.data = this.writeFileService.dataFormJson;
      this.arrQuestion = this.data.form.question.arrListQuestion;
    }
  }

}
