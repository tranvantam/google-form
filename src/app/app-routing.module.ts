import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import(`./modules/home/home.module`).then(m => m.HomeModule)
  }, 
  {
    path: 'create',
    loadChildren: () => import(`./modules/create/create.module`).then(m => m.CreateModule)
  },
  {
    path: 'preview',
    loadChildren: () => import(`./modules/preview/preview.module`).then(m => m.PreviewModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
